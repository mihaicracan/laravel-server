<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MyClass extends Model
{
	use SoftDeletes;

    protected $fillable = ['name'];
    protected $dates = ['deleted_at'];
    protected $table = 'classes';
}
