<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Token;

class AuthController extends Controller
{
    public function register(Request $request)
    {
    	User::create([
    		'name' => $request->input('name'),
    		'email' => $request->input('email'),
    		'hash' => md5($request->input('password')),
    		'role' => 'student'
    	]);

    	return response()->json([ 'status' => 'success' ]);
    }

    public function login(Request $request)
    {
    	$hash = md5($request->input('password'));
    	$email = $request->input('email');
    	$token = str_random(64);

    	$user = User::where([
    		'hash' => $hash,
    		'email' => $email
    	])->first();

    	if ($user) {
    		Token::create([
    			'userId' => $user->id,
    			'token' => $token
    		]);

    		return response()->json([ 
    			'status' => 'success',
    			'token' => $token,
    			'role' => $user->role,
    			'name' => $user->name
    		]);
    	}

    	return response()->json([ 'status' => 'error' ], 401);
    }
}
