<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Grade;

class GradesController extends Controller
{
    public function getItems(Request $request)
    {
        $items = Grade::all();   
        return response()->json([ 
            'status' => 'success',
            'data' => $items
        ]);
    }

    public function addItem(Request $request)   // aici am ramas
    {
        $item = Grade::create([
            'grade' => $request->input('grade'),
            'userId' => $request->input('userId'),
            'classId' => $request->input('classId')
        ]);

        return response()->json([ 
            'status' => 'success',
            'data' => $item
        ]);
    }

    public function updateItem(Request $request, $id)
    {
        Grade::findOrFail($id)->update([
            'grade' => $request->input('grade')
        ]);

        return response()->json([ 'status' => 'success' ]);
    }

    public function deleteItem(Request $request, $id)
    {
        Grade::findOrFail($id)->delete();

        return response()->json([ 'status' => 'success' ]);
    }
}
