<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\MyClass;

class ClassesController extends Controller
{
    public function getItems(Request $request)
    {
        $items = MyClass::all();

        return response()->json([ 
            'status' => 'success',
            'data' => $items
        ]);
    }

    public function addItem(Request $request)
    {
        $item = MyClass::create([
            'name' => $request->input('name')
        ]);

        return response()->json([ 
            'status' => 'success',
            'data' => $item
        ]);
    }

    public function updateItem(Request $request, $id)
    {
        MyClass::findOrFail($id)->update([
            'name' => $request->input('name')
        ]);

        return response()->json([ 'status' => 'success' ]);
    }

    public function deleteItem(Request $request, $id)
    {
        MyClass::findOrFail($id)->delete();

        return response()->json([ 'status' => 'success' ]);
    }
}
