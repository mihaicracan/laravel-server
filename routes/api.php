<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', 'AuthController@register');
Route::post('/login', 'AuthController@login');

Route::get('/classes', 'ClassesController@getItems');
Route::post('/classes', 'ClassesController@addItem');
Route::put('/classes/{id}', 'ClassesController@updateItem');
Route::delete('/classes/{id}', 'ClassesController@deleteItem');

Route::get('/grades', 'GradesController@getItems');
Route::post('/grades', 'GradesController@addItem');
Route::put('/grades/{id}', 'GradesController@updateItem');
Route::delete('/grades/{id}', 'GradesController@deleteItem');


